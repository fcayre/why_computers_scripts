#! /bin/bash

set -e

# Variables
dir="$(dirname "$0")"
# Inclusions des fichiers de fonctions
# . functions/tuxedo_kernel_amd

# Couleur pour la console
CYAN="\\033[1;96m"
PINK="\e[38;5;198m"
RED="\\033[1;31m"
GREEN="\e[038;5;82m"
RESETCOLOR="\\033[0;0m"

# Verification des permissions root
if [ $(whoami) != 'root' ];then
    echo -e  ${RED}"Ce script a besoin des permissions administrateur (sudo), veuillez le lancer de la manière suivante : sudo ./script.sh"${RESETCOLOR}
    exit 0
fi

echo -e ${CYAN}""
cat $dir/resources/why_text
echo -e ""${RESETCOLOR}

sleep 4


# Verification de la connexion internet
echo -en ${CYAN}"Contrôle de la connexion internet..."${RESETCOLOR}
wget -q --spider http://ch.archive.ubuntu.com/ubuntu/dists/
if [ $? -eq 0 ]; then
    echo -e ${CYAN}"\e[0K\rContrôle de la connexion internet"${GREEN}"    [OK]"${RESETCOLOR} # \e[0K\r = retour en début de ligne
else
    echo -e ${CYAN}"\e[0K\rContrôle de la connexion internet"${RED}"    [KO]"${RESETCOLOR}
    echo -e ${RED}"\nVeuillez vérifier votre connexion internet"${RESETCOLOR}
    exit 0
fi

# Active Canonical ppa + update des dépôts
echo -e ${CYAN}"Activation du dépôt canonical..."${RESETCOLOR}
sed -i -e '/# deb http:\/\/archive.canonical.com\/ubuntu\ /s/# //' /etc/apt/sources.list
apt update

# Installation des applications favorites de la liste apps_to_install.list
echo -e ${CYAN}"Installation des applications..."${RESETCOLOR}
FAVS="$dir/resources/apps_to_install.list"
apt install -y --no-install-recommends $(cat $FAVS)
apt --fix-broken install
apt autoremove -y

# Configuration initiale de libdvd-pkg
dpkg-reconfigure libdvd-pkg

# installation fonts Microsoft avec paquet .deb car il y a souvent des erreurs avec le paquet Ubuntu
echo -e ${CYAN}"Installation des fonts Microsoft..."${RESETCOLOR}
apt install -y ttf-mscorefonts-installer

# Modification du fond d'écran
echo -e ${CYAN}"Modification du fond d'écran..."${RESETCOLOR} 
mkdir wall
cd wall
echo -e ${CYAN}"Téléchargement de l'image..."${RESETCOLOR} 
wget -c https://oem.whyopencomputing.ch/why_wallpaper_fhd.jpg
cp why_wallpaper_fhd.jpg /usr/share/backgrounds/why_wallpaper_fhd.jpg
cd ..
rm -rf wall/
cp -r $dir/resources/10_ubuntu-settings.gschema.override /usr/share/glib-2.0/schemas/10_ubuntu-settings.gschema.override
glib-compile-schemas /usr/share/glib-2.0/schemas/

# Modification des application du dock
echo -e ${CYAN}"Modification des application du dock..."${RESETCOLOR} 
cp -r $dir/resources/99_launcher.favorites.gschema.override /usr/share/glib-2.0/schemas/99_launcher.favorites.gschema.override
glib-compile-schemas /usr/share/glib-2.0/schemas/

# Installation du script Brother pour l'installation des pilotes d'impression
echo -e ${CYAN}"Installation du script Brother..."${RESETCOLOR}
cp $dir/resources/brother/linux-brprinter-installer-2.2.0-1 /usr/local/bin/
cp $dir/resources/brother/brotherInstall.desktop /usr/share/applications/
cp $dir/resources/brother/printer.png /usr/share/applications/
chmod a+rx /usr/local/bin/linux-brprinter-installer-2.2.0-1

# Modification de grub pour ne pas afficher le menu
echo -e ${CYAN}"Modification de grub (menu)..."${RESETCOLOR}
sed -i '/^GRUB_TIMEOUT=/c\GRUB_TIMEOUT=0' /etc/default/grub
update-grub

# Installation du Kernel 5.10.2-051002
# mkdir kernel_install
# cd kernel_install
# wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.9.16/amd64/linux-headers-5.9.16-050916_5.9.16-050916.202012211331_all.deb
# wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.9.16/amd64/linux-headers-5.9.16-050916-generic_5.9.16-050916.202012211331_amd64.deb
# wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.9.16/amd64/linux-image-unsigned-5.9.16-050916-generic_5.9.16-050916.202012211331_amd64.deb
# wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.9.16/amd64/linux-modules-5.9.16-050916-generic_5.9.16-050916.202012211331_amd64.deb
# sudo dpkg -i *.deb
# cd ..
# rm -rf kernel_install/

# Updates
apt update
apt upgrade -y
apt full-upgrade -y

echo -e ${CYAN}""
cat $dir/resources/why_text
echo -e ""${RESETCOLOR}

echo -e ${GREEN}"TERMINÉ ! Redémarrez, vérifiez puis finalisez l'installation OEM."${RESETCOLOR}
